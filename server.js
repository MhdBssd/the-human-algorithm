const express     = require('express');
const path        = require('path');
const fs          = require('fs');
const Fuse        = require('fuse.js');

const port = process.env.PORT || 8000;
const app = express();

const networksFiles = JSON.parse(fs.readFileSync(`${__dirname}/public/networks/summary.json`));
const fuse = new Fuse(networksFiles, {
  keys: ['nodes.name', 'title'],
  includeScore: true,
  minMatchCharLength: 3,
  threshold: 0.1,
  ignoreLocation: true,
  includeMatches: true,
});


app.use(express.static(`${__dirname}/public`));

app.get('/search', (req, res) => {
  const searchQuery = req.query.q;
  const results = fuse.search(searchQuery).filter(({ score }) => score <= 0.1);
  const titles = results?.length ? results.map(({ item }) => item.title) : [];

  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET');
  res.header('Access-Control-Allow-Headers', 'Content-Type');

  res.send(titles);
});

app.get('*', (req, res) => {
  res.sendFile(path.resolve(__dirname, 'public/index.html'));
});

app.listen(port);
