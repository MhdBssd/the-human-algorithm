const smallCircleNodes = ['FinalInfo', 'Year', 'YearFinal', 'Final'];
const largeCircleNodes = ['Person', 'Institution', 'MergedInstitution'];

const brokenLinks = ['BlockedFailed', 'BlockedFailedTransaction'];
const solidLinks = ['Association', 'BlockedFailed', 'Connection', 'Final', 'InfluenceControl', 'SaleProperty', 'YearArrow', 'YearLine'];
const dashedLinks = ['BlockedFailedTransaction', 'FinancialAssociation', 'FinancialConnection', 'FinancialTransaction'];
const directionalLinks = ['Association', 'FinancialAssociation', 'FinancialTransaction', 'InfluenceControl', 'SaleTransfer'];
const bidirectionalLinks = ['Association', 'FinancialAssociation'];

const linksTypes = [...new Set([...brokenLinks, ...solidLinks, ...dashedLinks, ...directionalLinks, ...bidirectionalLinks])];
const nodesTypes = [...smallCircleNodes, ...largeCircleNodes];

const linksColorMap = {
  Default: 'white',
  Association: 'slateblue',
  BlockedFailed: 'red',
  BlockedFailedTransaction: 'lightcoral',
  Connection: 'dodgerblue',
  FinancialConnection: 'cadetblue',
  Final: 'seagreen',
  FinancialAssociation: 'lightseagreen',
  FinancialTransaction: 'lawngreen',
  InfluenceControl: 'orange',
  SaleProperty: 'greenyellow',
  SaleTransfer: 'springgreen',
  YearArrow: 'turquoise',
  YearLine: 'turquoise'
};
const nodesColorMap = {
  Default: { fill: 'var(--dark-overlay-background)', stroke: 'white' },
  Person: { stroke: '#281251' },
  Institution: { stroke: 'darkred' },
  MergedInstitution: { stroke: 'darkred' },
  FinalInfo: { stroke: 'springgreen' },
  Final: { stroke: 'seagreen' },
  Year: { stroke: 'turquoise' },
  YearFinal: { stroke: 'seagreen' }
};

const linksStyles = linksTypes.map(linkType => ({
  type: linkType,
  fillColor: false,
  strokeWidth: 5,
  strokeDashArray: dashedLinks.includes(linkType) ? 10 : 0,
  strokeColor: linksColorMap[linkType] ?? linksColorMap.Default,
  markerStart: bidirectionalLinks.includes(linkType)
    ? `url(#arrow-${linkType.toLowerCase()})`
    : '',
  markerEnd: directionalLinks.includes(linkType)
    ? `url(#arrow-${linkType.toLowerCase()})`
    : brokenLinks.includes(linkType)
      ? 'url(#square)'
      : ''
}));
const nodesStyles = nodesTypes.map(nodeType => ({
  type: nodeType,
  radius: largeCircleNodes.includes(nodeType) ? 50 : 15,
  fillColor: nodesColorMap[nodeType].fill ?? nodesColorMap.Default.fill,
  strokeColor: nodesColorMap[nodeType].stroke ?? nodesColorMap.Default.stroke,
  strokeWidth: 15
}));


export default { linksStyles, nodesStyles };
