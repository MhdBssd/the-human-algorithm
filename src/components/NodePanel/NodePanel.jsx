import React, { useState }          from 'react';

import styles                       from './styles.module.scss';
import solidLine                    from '../../../public/assets/solid-line.svg';
import dashedLine                   from '../../../public/assets/dashed-line.svg';
import SolidSquare                  from '../../../public/assets/solid-square.svg';
import LineCircle                   from '../../../public/assets/circle-line.svg';
import solidDirectionalArrow        from '../../../public/assets/icons/directional-line-arrow.svg';
import dashedDirectionalArrow       from '../../../public/assets/icons/directional-line-dashed.svg';
import solidBidirectionalArrow      from '../../../public/assets/icons/bidirectional-line-arrow.svg';
import dashedBidirectionalArrow     from '../../../public/assets/icons/bidirectional-line-arrow-dashed.svg';
import lineCircleInnerDashed        from '../../../public/assets/icons/circle-line-inner-dashed.svg';
import lineSquare                   from '../../../public/assets/icons/line-square.svg';
import entitiesStyles               from '../../../entitiesStyles.js';


const solidLinks = ['Association', 'BlockedFailed', 'Connection', 'Final', 'InfluenceControl', 'SaleProperty', 'Year', 'YearArrow'];
const dashedLinks = ['BlockedFailedTransaction', 'FinancialAssociation', 'FinancialConnection', 'FinancialTransaction'];
const directionalLinks = ['FinancialTransaction', 'InfluenceControl', 'SaleTransfer'];
const bidirectionalLinks = ['Association', 'FinancialAssociation'];
const circleLinks = ['BlockedFailed', 'BlockedFailedTransaction'];

const NodePanel = ({ node = {}, selectedNodeConnections = { to: [], from: [] }, connectionsCounter, onNodeSelected }) => {
  const isDesktop = window.innerWidth > 1300;
  const [ isConnectionsListOpen, setIsConnnectionListOpen ] = useState(isDesktop);

  if (!node.id) return ( <section className={ styles.panelContainer }></section> );

  return (
    <section
      onClick={ e => e.stopPropagation() }
      onMouseDown={ e => e.preventDefault() }
      className={ `${styles.panelContainerOpen} ${isConnectionsListOpen && styles.panelContainerListOpen}` }>
      <header className={ styles.panelHeader }>
        <h2 className={ styles.nodeName }>
          { node.name }
        </h2>
        <h3 className={ styles.nodeType }>
          <span
            style={{ fill: entitiesStyles.nodesStyles.find(({ type }) => type === node.type).strokeColor }}
            dangerouslySetInnerHTML={{ __html: getNodeTypeIcon(node.type) }}>
          </span>
          { node.type }
        </h3>
        <button
          onClick={ () => setIsConnnectionListOpen(!isConnectionsListOpen) }
          className={ styles.connectionsToggle }
          type="button">
          { `${connectionsCounter} ${connectionsCounter > 1 ? 'connections' : 'connection'}` }
          <svg xmlns="http://www.w3.org/2000/svg" height="24" width="24">
            <path d="M7.41 8.59L12 13.17l4.59-4.58L18 10l-6 6-6-6 1.41-1.41z"/>
          </svg>
        </button>
      </header>

      <dl className={
          isDesktop
            ? styles.connectionsListOpen
            : isConnectionsListOpen
              ? styles.connectionsListOpen
              : styles.connectionsList
        }>
        {
          Object.entries(selectedNodeConnections.to).map(([ connectionType, connectionTargets ], index) => (
            <div
              key={ `${connectionType}-${index}` }
              className={ styles.connectionItem }>
              <span
                className={ styles.connectionTypeIcon }
                style={{ fill: `${entitiesStyles.linksStyles.find(link => link.type === connectionType).strokeColor}` }}
                dangerouslySetInnerHTML={{ __html: getConnectionTypeIcon(connectionType) }}>
              </span>
              <dt className={ styles.connectionType }>
                { `${connectionType} ${getConnectionVocabulary(connectionType, 'to')}: ` }
              </dt>
              {
                connectionTargets.map((target, index) => (
                  <dd
                    onClick={ () => onNodeSelected(target.id) }
                    className={ styles.connectionName }
                    key={ `${target.id}-${index}` }>
                      { target.name }
                  </dd>
                ))
              }
            </div>
          ))
        }
        {
          Object.entries(selectedNodeConnections.from).map(([ connectionType, connectionSources ], index) => (
            <div
              key={ `${connectionType}-${index}` }
              className={ styles.connectionItem }>
              <span
                className={ styles.connectionTypeIcon }
                style={{ fill: `${entitiesStyles.linksStyles.find(link => link.type === connectionType).strokeColor}` }}
                dangerouslySetInnerHTML={{ __html: getConnectionTypeIcon(connectionType) }}>
              </span>
              <dt className={ styles.connectionType }>
                { `${connectionType} ${getConnectionVocabulary(connectionType, 'from')}: ` }
              </dt>
              {
                connectionSources.map((source, index) => (
                  <dd
                    onClick={ () => onNodeSelected(source.id) }
                    className={ styles.connectionName }
                    key={ `${source.id}-${index}` }>
                    { source.name }
                  </dd>
                ))
              }
            </div>
          ))
        }
      </dl>
    </section>
  );
};


function getNodeTypeIcon(nodeType) {
  if (['Institution', 'Person'].includes(nodeType)) return LineCircle;
  if (['MergedInstitution'].includes(nodeType)) return lineCircleInnerDashed;

  return SolidSquare;
}

function getConnectionTypeIcon(connectionType) {
  const isSolidLine = solidLinks.includes(connectionType);

  if (isSolidLine) {
    if (circleLinks.includes(connectionType)) return lineSquare;
    if (directionalLinks.includes(connectionType)) return solidDirectionalArrow;
    if (bidirectionalLinks.includes(connectionType)) return solidBidirectionalArrow;

    return solidLine;
  } else {
    if (directionalLinks.includes(connectionType)) return dashedDirectionalArrow;
    if (bidirectionalLinks.includes(connectionType)) return dashedBidirectionalArrow;

    return dashedLine;
  }
}

function getConnectionVocabulary(connectionType, origin) {
  switch (connectionType) {
    case 'Association':
    case 'FinancialAssociation':
      return 'with';
    case 'Final':
    case 'FinalInfo':
      return '';
    case 'InfluenceControl':
      return origin === 'from' ? 'by' : 'over';
    default:
      return origin;
  }
}


export default NodePanel;
