import React      from 'react';

import styles     from './styles.module.scss';


const Header = ({ networkTitle, onToggleNavBar, onNavigateToNextNetwork }) => (
  <header className={ styles.headerContainer }>
    <button onClick={ () => onToggleNavBar() } type="button" className={ styles.headerControls }>
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
        <path d="M0 0h24v24H0z" fill="lemonchiffon"/>
        <path d="M3 13h2v-2H3v2zm0 4h2v-2H3v2zm0-8h2V7H3v2zm4 4h14v-2H7v2zm0 4h14v-2H7v2zM7 7v2h14V7H7z"/>
      </svg>
    </button>

    <h1 className={ styles.headerTitle }>{ networkTitle }</h1>

    <button onClick={ () => onNavigateToNextNetwork(-1)} type="button" className={ styles.headerControls }>
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
        <path d="M0 0h24v24H0z" fill="lemonchiffon"/>
        <path d="M6 6h2v12H6zm3.5 6l8.5 6V6z"/>
      </svg>
    </button>

    <button onClick={ () => onNavigateToNextNetwork(1)} type="button" className={ styles.headerControls }>
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
        <path d="M0 0h24v24H0z" fill="lemonchiffon"/>
        <path d="M6 18l8.5-6L6 6v12zM16 6v12h2V6h-2z"/>
      </svg>
    </button>
  </header>
);


export default Header;
