import React      from 'react';

import styles     from './styles.module.scss';


const Loader = ({ text = 'Loading...', className = ''}) => {
  return (
    <div className={ `${styles.loader}${className ? ` ${className}` : '' }` }>
      <div className={ styles.loaderDots }>
        <div className={ styles.loaderDot }></div>
        <div className={ styles.loaderDot }></div>
        <div className={ styles.loaderDot }></div>
        <div className={ styles.loaderDot }></div>
        <div className={ styles.loaderDot }></div>
        <div className={ styles.loaderDot }></div>
      </div>
      <span className={ styles.loaderText }>{ text }</span>
    </div>
  );
};


export default Loader;
