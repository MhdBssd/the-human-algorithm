import React, {
  useState,
  useEffect,
  useCallback,
}                     from 'react';
import debounce       from 'lodash.debounce';

import styles         from './styles.module.scss';
import Logo           from '../../../public/assets/logo.svg';
import SearchBar      from '../SearchBar/SearchBar.jsx';
import Loader         from '../Loader/Loader.jsx';


const NavBar = ({ isOpen, networksList, currentNetworkId, onNetworkSelect, onToggleNavBar }) => {
  const [ searchInput, setSearchInput ] = useState('');
  const [ matchingInputNetwork, setMatchingInputNetwork ] = useState([]);
  const [ isLoading, setIsLoading ] = useState(false);
  const filteredNetworkList = searchInput ? matchingInputNetwork : networksList;

  useEffect(() => {
    if (!searchInput) return;

    setIsLoading(true);
    debouncedSearchForTerm(searchInput,
      matchingNetworks => setMatchingInputNetwork(networksList.filter(({ title }) => matchingNetworks.includes(title))),
      error => setIsLoading(false),
      final => setIsLoading(false))
  }, [ searchInput ]);
  useEffect(() => preventBodyScroll(), [ isOpen ]);

  const preventBodyScroll = useCallback(() => {
    const isMobile = window.innerWidth < 768;

    if (isOpen && isMobile) document.body.style.overflow = 'hidden';

    return () => document.body.style.overflow = 'initial';
  }, []);

  return (
    <nav className={ isOpen ? styles.navBarContainerOpen : styles.navBarContainer }>
      <span className={ styles.logo } dangerouslySetInnerHTML={{__html: Logo}} />

      <h1 className={ styles.siteTitle }>The Human Algorithm</h1>

      <button onClick={ () => onToggleNavBar() } className={ styles.closeIcon } type="button">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
          <path d="M0 0h24v24H0z" fill="lemonchiffon"/>
          <path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"/>
        </svg>
      </button>

      <p className={ styles.siteDescription }>
        Mark Lombardi (1951 – 2000) was an American neo-conceptual artist who specialized in drawings that document alleged financial and political frauds by power brokers, and in general "the uses and abuses of power".
        <a href="https://wikipedia.org/wiki/Mark_Lombardi" target="_blank" className={ styles.wikipediaLink }>Wikipedia</a>
      </p>

      <div className={ styles.searchBarWrapper }>
        <SearchBar
          searchInput={ searchInput }
          setSearchInput={ setSearchInput }>
        </SearchBar>
      </div>

      {
        isLoading
          ? <Loader className={ styles.loader }></Loader>
          :
            filteredNetworkList.length
              ?
                <ol className={ styles.networkList }>
                  { filteredNetworkList.map(({ title, id }) => (
                      <li key={ id }>
                        <button
                          onClick={ () => { onNetworkSelect(id); onToggleNavBar(); } }
                          className={ currentNetworkId === id ? styles.networkTitleSelected : styles.networkTitle }
                          type="button">
                          { title }
                        </button>
                      </li>
                    )) }
                </ol>
              : <span className={ styles.noResultsMessage }>
                  No data is matching your search
                </span>
      }
    </nav>
  );
};



const debouncedSearchForTerm = debounce(async (input, resolve, reject, final) => {
  const url = `http://localhost:8000/search?${new URLSearchParams({ q: input })}`;
  const options = { method: 'GET', headers: { 'Accept': 'application/json' } };

  try {
    const response = await (await fetch(url, options)).json();

    resolve(response);
  } catch (error) {
    reject(error);
  } finally {
    final()
  }
}, 500);


export default NavBar;
