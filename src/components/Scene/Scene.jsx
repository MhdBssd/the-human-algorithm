import {
  select,
  drag,
  forceCenter,
  forceRadial,
  forceCollide,
  forceSimulation,
  forceLink,
  event,
  zoom,
}                               from 'd3';
import React, { useEffect }     from 'react';

import entitiesStyles           from '../../../entitiesStyles.js';
import styles                   from './styles.module.scss';


const height = 2000;
const width = 2000;
const arrowheadLength = 15;
const markerSizeInPixels = 15;
const markerXOffset = 6;
const markerYOffset = 6;
const scrollToSceneCenter = () => window.scrollTo(width / 3, height / 3);
const centerForce = forceCenter(width / 2, height / 2);
const radialForce = forceRadial(1500, width / 2, height / 2).strength(.25);
const collisionForce = forceCollide(150).strength(.75);
const linksForce = links => forceLink(links)
  .id(link => link.id)
  .distance(link => Math.min(...[link.source.radius * 4, link.target.radius * 4], 2000))
  .strength(.5);
const createForceSimulation = (nodes, links) => forceSimulation(nodes)
  .force('center', centerForce)
  .force('radial', radialForce)
  .force('collide', collisionForce)
  .force('link', linksForce(links))
  .alphaTarget(0.05);

const Scene = ({ network, onNodeSelected, selectedNodeId, selectedNodeConnectedNodesIds }) => {
  useEffect(() => addMarkers(select('#scene')), []);
  useEffect(() => createNetwork({ network, onNodeSelected }), [ network ]);
  useEffect(() => scrollToSceneCenter(), [ network ]);
  useEffect(() => selectedNodeId && styleSelectedNode({ container: select('#container'), selectedNodeId, selectedNodeConnectedNodesIds }), [ selectedNodeId ]);

  return (
    <svg id="scene" className={styles.sceneContainer}>
      <g id="container" width={2000} height={2000}></g>
    </svg>
  );
};


function createNetwork({ network, onNodeSelected }) {
  const links = network.links.map(link => ({
    ...link,
    ...entitiesStyles.linksStyles.find(style => style.type === link.type)
  }));
  const nodes = network.nodes.map(node => {
    const nodeStyle = entitiesStyles.nodesStyles.find(style => style.type === node.type);

    return {
      ...node,
      ...nodeStyle,
      radius: Math.max(nodeStyle.radius, Math.min(nodeStyle.radius * 2, 30 * getNodeConnectionsCount(node, network.links)))
    };
} );
  const container = select('#container');
  const scene = select('#scene')
    .attr('viewBox', [width / -2, height / -2, width * 2, height * 2])
    .attr('width', '100%')
    .attr('height', '100%')
      .call(zoom()
        .on('zoom', onZoom(container))
        .scaleExtent([.5, 5])
      );
  const simulation = createForceSimulation(nodes, links)
    .on('tick', () => onSimulationTick(nodesElements, linksElements));
  const linksElements = mergeDataWithElements({ container, tag: 'line', data: links })
    .call(styleLinks);
  const nodesElements = mergeDataWithElements({ container, tag: 'g', data: nodes, keyFunction: d => d.id })
    .call(styleNodes, network)
    .call(onDrag(simulation))
    .on('click', d => { onNodeSelected(d.id); event.stopPropagation(); });

  return () => { nodesElements.on('click', null); };
}


function onZoom(container) {
  return () => container.attr('transform', event.transform);
}

function addMarkers(container) {
  container.append('svg:defs').append('svg:marker')
    .attr('id', 'square')
    .attr('refX', markerXOffset)
    .attr('refY', markerYOffset)
    .attr('markerWidth', markerSizeInPixels)
    .attr('markerHeight', markerSizeInPixels)
    .attr('orient', 'auto-start-reverse')
    .append('path')
      .attr('d', 'M0 0h12v12H0z')
      .style('fill', 'red');

  entitiesStyles.linksStyles.forEach(linkStyle => {
    container.append('svg:defs').append('svg:marker')
      .attr('id', `arrow-${linkStyle.type.toLowerCase()}`)
      .attr('refX', markerXOffset)
      .attr('refY', markerYOffset)
      .attr('markerWidth', markerSizeInPixels)
      .attr('markerHeight', markerSizeInPixels)
      .attr('orient', linkStyle.markerStart ? 'auto-start-reverse' : 'auto')
      .append('path')
        .attr('d', 'M 0 0 12 6 0 12 3 6')
        .style('fill', linkStyle.strokeColor);
  });
}

function styleLinks(linksSelection) {
  linksSelection
    .attr('stroke', ({ strokeColor }) => strokeColor)
    .attr('stroke-width', ({ strokeWidth }) => strokeWidth)
    .attr('stroke-dasharray', ({ strokeDashArray }) => strokeDashArray)
    .attr('fill', ({ fillColor }) => fillColor)
    .attr('marker-start', ({ markerStart }) => markerStart)
    .attr('marker-end', ({ markerEnd }) => markerEnd);
}

function styleNodes(nodesSelection, network) {
  nodesSelection
    .append('circle')
    .attr('r', ({ radius }) => radius)
    .attr('stroke', ({ strokeColor }) => strokeColor)
    .attr('stroke-width', ({ strokeWidth }) => strokeWidth)
    .attr('fill', ({ fillColor }) => fillColor);

  nodesSelection
    .filter(node => node.type === 'MergedInstitution')
    .append('circle')
    .attr('r', ({ radius }) => radius / 2)
    .attr('stroke', ({ strokeColor }) => strokeColor)
    .attr('stroke-width', '.5rem')
    .attr('stroke-dasharray', '.5rem')
    .attr('fill','hsla(250, 50%, 9%, .9)');

  nodesSelection
    .append('text')
    .text(d => d.name)
    .attr('fill', 'white')
    .attr('font-size', d => `${Math.cbrt(getNodeConnectionsCount(d, network.links))}rem`)
    .attr('font-weight', 600)
    .attr('text-anchor', 'middle');

  nodesSelection
    .filter(d => !!d.location)
    .insert('text', this)
    .text(d => d.location)
    .attr('fill', 'white')
    .attr('text-anchor', 'middle')
    .attr('font-size', '1.25rem')
    .attr('font-style', 'italic')
    .attr('y', 25);
}

function styleSelectedNode({ container, selectedNodeId, selectedNodeConnectedNodesIds }) {
  const selectedNode = container.selectAll('g').filter(({ id }) => id === selectedNodeId);
  const unrelatedLinks = container.selectAll('line').filter(({ source, target }) => ![ source.id, target.id ].includes(selectedNodeId));
  const unRelatedNodes = container.selectAll('g').filter(d => (d.id !== selectedNodeId) && !selectedNodeConnectedNodesIds.includes(d.id));

  selectedNode.selectAll('text').attr('fill', 'royalblue');
  unRelatedNodes.style('opacity', .15);
  unrelatedLinks.style('opacity', .15);

  return () => {
    selectedNode.selectAll('text').attr('fill', 'white');
    unRelatedNodes.style('opacity', 1);
    unrelatedLinks.style('opacity', 1);
  }
}

function mergeDataWithElements({ container, tag, data, keyFunction = null }) {
  return container
    .selectAll(tag)
    .data(data, keyFunction)
    .join(tag);
}

function onDrag(simulation) {
  return drag()
    .on('start', d => onDragStarted(d, simulation))
    .on('drag', d => onDragged(d, simulation))
    .on('end', d => onDragEnded(d, simulation));
}

function onDragStarted(d, simulation) {
  simulation.restart();
  simulation.alphaTarget(0.25);

  d.fx = d.x;
  d.fy = d.y;
}

function onDragged(d) {
  d.fx = event.x;
  d.fy = event.y;
}

function onDragEnded(d, simulation) {
  simulation.alphaTarget(0.05);

  d.fx = null;
  d.fy = null;
}

function onSimulationTick(nodes, links) {
  nodes
    .style('transform', ({ x, y }) => `translate(${x}px, ${y}px)`);
  links
    .attr('x1', ({ source, target, markerStart }) => source.x + Math.cos(getAngle({ source, target })) * ((source.radius + (source.strokeWidth + markerXOffset)) + (markerStart ? arrowheadLength : -arrowheadLength)))
    .attr('y1', ({ source, target, markerStart }) => source.y + Math.sin(getAngle({ source, target })) * ((source.radius + (source.strokeWidth + markerXOffset)) + (markerStart ? arrowheadLength : -arrowheadLength)))
    .attr('x2', ({ source, target, markerEnd }) => target.x - Math.cos(getAngle({ source, target })) * ((target.radius + (target.strokeWidth + markerXOffset)) + (markerEnd ? arrowheadLength : -arrowheadLength)))
    .attr('y2', ({ source, target, markerEnd }) => target.y - Math.sin(getAngle({ source, target })) * ((target.radius + (target.strokeWidth + markerXOffset)) + (markerEnd ? arrowheadLength : -arrowheadLength)));
}

function getAngle({ source, target }) {
  return Math.atan2(target.y - source.y, target.x - source.x);
}

function getNodeConnectionsCount(node, links) {
  const nodeConnections = links?.filter(({ source, target }) => ([ source, target ].includes(node.id)));

  return nodeConnections?.length ?? 0;
}


export default Scene;
