import React, { useState }      from 'react';

import useNetwork               from '../../hooks/useNetwork.js';
import useSelectedNode          from '../../hooks/useSelectedNode.js';
import networksMetaData         from '../../../public/networks/meta.json';
import Header                   from '../Header/Header.jsx';
import NavBar                   from '../NavBar/NavBar.jsx';
import NodePanel                from '../NodePanel/NodePanel.jsx';
import Loader                   from '../Loader/Loader.jsx';
import Scene                    from '../Scene/Scene.jsx';
import styles                   from './styles.module.scss';
import                               '../../styles/main.scss';


const getRandomIntegerBetween = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min;
const networksIds = networksMetaData.map(({ id }) => id);
const [ minNetworkId, maxNetworkId ] = [ Math.min(...networksIds), Math.max(...networksIds) ];
const initialNetworkId = getRandomIntegerBetween(minNetworkId, maxNetworkId);

const App = () => {
  const [ isNavbarOpen, setIsNavbarOpen ] = useState(false);
  const { currentNetwork, isLoadingNetwork, setCurrentNetworkId, onNavigateToNextNetwork } = useNetwork(initialNetworkId, networksMetaData);
  const { currentSelectedNode, selectedNodeConnectedNodesIds, selectedNodeConnectionsByTypes, onNodeSelected } = useSelectedNode(currentNetwork);

  return (
    <>
      { !currentSelectedNode.id &&
        <Header
          networkTitle={ currentNetwork.title }
          onNavigateToNextNetwork={ onNavigateToNextNetwork }
          onToggleNavBar= { () => setIsNavbarOpen(!isNavbarOpen) }>
        </Header>
      }

      <NavBar
        isOpen={ isNavbarOpen }
        networksList={ networksMetaData }
        currentNetworkId={ currentNetwork.id }
        onToggleNavBar= { () => setIsNavbarOpen(!isNavbarOpen) }
        onNetworkSelect={ setCurrentNetworkId }>
      </NavBar>

      {
        isLoadingNetwork
          ?
          <Loader text={'Loading network...'}></Loader>
          :
          <main
            onClick={ () => { isNavbarOpen && setIsNavbarOpen(false); onNodeSelected(null); } }
            className={ styles.appContainer }>
            <Scene
              network={ currentNetwork }
              selectedNodeId={ currentSelectedNode.id }
              selectedNodeConnectedNodesIds={ selectedNodeConnectedNodesIds }
              onNodeSelected={ onNodeSelected }>
            </Scene>
            <NodePanel
              node={ currentSelectedNode }
              connectionsCounter={ selectedNodeConnectedNodesIds.length }
              selectedNodeConnections={ selectedNodeConnectionsByTypes }
              onNodeSelected={ onNodeSelected }>
            </NodePanel>
          </main>
      }
    </>
  );
};


export default App;
