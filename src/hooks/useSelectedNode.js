import {
  useState,
  useMemo,
  useEffect,
  useCallback
}                 from 'react';


const useSelectedNode = network => {
  const [ currentSelectedNode, setCurrentSelectedNode ] = useState({});
  const selectedNodeConnections = useMemo(() => getNodeConnectionsByOrigin(currentSelectedNode.id, network), [ currentSelectedNode ]);
  const selectedNodeConnectionsByTypes = useMemo(() => getConnectionsTypesMap(network, selectedNodeConnections), [ currentSelectedNode ]);
  const selectedNodeConnectedNodesIds = useMemo(() => getSelectedNodeConnectionsIds(selectedNodeConnections), [ currentSelectedNode ]);
  const onNodeSelected = useCallback(nodeId => setCurrentSelectedNode(({ id: previousNodeId }) => (nodeId && nodeId !== previousNodeId) ? getNodeById(network, nodeId) : {}));

  useEffect(() => { setCurrentSelectedNode({}) }, [ network ]);

  return {
    currentSelectedNode,
    selectedNodeConnectionsByTypes,
    selectedNodeConnectedNodesIds,
    onNodeSelected
  };
};


function getSelectedNodeConnectionsIds(selectedNodeConnections) {
  return [
    ...new Set([
      ...Object.values(selectedNodeConnections.to).flatMap(({ target }) => target),
      ...Object.values(selectedNodeConnections.from).flatMap(({ source }) => source)
    ])
  ];
}

function getNodeById(network, id) {
  return network?.nodes.find(node => id === node.id) ?? {};
}

function getNodeConnectionsByOrigin(nodeId, network = {}) {
  const nodeConnections = { from: [], to: [] };

  if (!nodeId || !network?.links?.length) return nodeConnections;

  nodeConnections.from = network.links.filter(link => link.target === nodeId);
  nodeConnections.to = network.links.filter(link => link.source === nodeId);

  return nodeConnections;
}

function getConnectionsTypesMap(network, { to, from }) {
  return {
    to: to.reduce((acc, { type, target }) => ({ ...acc, [type]: [ getNodeById(network, target), ...(acc[type] ?? []) ] }), {}),
    from: from.reduce((acc, { type, source }) => ({ ...acc, [type]: [ getNodeById(network, source), ...(acc[type] ?? []) ] }), {})
  };
}


export default useSelectedNode;
