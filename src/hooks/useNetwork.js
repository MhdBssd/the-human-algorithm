import {
  useState,
  useEffect,
  useCallback
}               from 'react';


const useNetwork = (networkId, networksMetadata) => {
  const [ isLoadingNetwork, setIsLoadingNetwork ] = useState(true);
  const [ currentNetworkId, setCurrentNetworkId ] = useState(networkId);
  const [ currentNetwork, setCurrentNetwork ] = useState({});

  const importNetwork = useCallback(async networkId => {
    try {
      setIsLoadingNetwork(true);
      setCurrentNetwork(await getNetwork(networkId));
    } catch (error) {
      throw error;
    } finally {
      setIsLoadingNetwork(false);
    }
  }, []);
  const onNavigateToNextNetwork = useCallback((navigateBy = 1) => {
    const minIndex = 0;
    const maxIndex = networksMetadata.length - 1;
    let nextNetworkIndex = networksMetadata.findIndex(({ title }) => title === currentNetwork.title) + navigateBy;

    if (nextNetworkIndex > maxIndex) nextNetworkIndex = minIndex;
    if (nextNetworkIndex < minIndex) nextNetworkIndex = maxIndex;

    setCurrentNetworkId(networksMetadata[nextNetworkIndex].id);
  }, [ currentNetwork ]);

  useEffect(() => { importNetwork(currentNetworkId) }, [ currentNetworkId ]);

  return {
    currentNetwork,
    isLoadingNetwork,
    setCurrentNetworkId,
    onNavigateToNextNetwork
  }
};


async function getNetwork(networkId) {
  const network = await import(
    /* webpackInclude: /\.json$/ */
    /* webpackExclude: /\.noimport\.json$/ */
    /* webpackChunkName: "network" */
    /* webpackMode: "lazy" */
    /* webpackPreload: true */
    `../../public/networks/${networkId}.json`
  );
  const currentNetwork = {
    ...network,
    id: networkId,
    nodes: network.nodes.map(node => ({
      ...node,
      type: getEntityTypeFromURL(node.type),
      name: getNodeName(node.name),
      location: getNodeLocation(node.name)
    })),
    links: network.links.map(link => ({
      ...link,
      type: getEntityTypeFromURL(link.type)
    }))
  };

  return currentNetwork;
}

function getNodeName(name) {
  const location = getNodeLocation(name);

  return location ? name.replace(location, '') : name;
}

function getNodeLocation(name) {
  const getEverythingBetweenHyphens = / - ([^-]+) -/gm;
  const location = name.match(getEverythingBetweenHyphens);

  return location && location[0];
}

function getEntityTypeFromURL(typeURL) {
  try {
    const { hash } = new URL(typeURL);
    const entityType = hash.replace('#', '');

    return entityType;
  } catch (error) {
    return '';
  }
}


export default useNetwork;
